# 校园综合服务平台小程序

#### 介绍
第一次接触nodejs，有不好的地方请别喷我，谢谢。
介绍不多说，直接上图<br>
#### 项目链接
[1.后台服务端地址：https://github.com/landalfYao/helpserver.git](https://github.com/landalfYao/helpserver.git)<br>
[2.小程序端地址：https://github.com/landalfYao/help.git](https://github.com/landalfYao/help.git)<br>
[3.后台客户端地址：https://github.com/landalfYao/helpclient.git](https://github.com/landalfYao/helpclient.git)<br>

#### 线上部署教程
1. 后台服务端部署教程[https://my.oschina.net/u/4053979/blog/3026392](https://my.oschina.net/u/4053979/blog/3026392)
#### 交流
QQ群<br>
- 1号群:664588763
- 2号群:939216531

小程序端：<br>
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101137_7de2cb10_1930998.jpeg "微信图片_201903200955137.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101147_ea79deb1_1930998.jpeg "微信图片_201903200955136.jpg")
![输入图片说明](http://ht.paopaole.cn/images/ppl.jpg)

![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101259_965d1848_1930998.jpeg "微信图片_201903200955132.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101318_6a3ed28b_1930998.jpeg "微信图片_201903200955133.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101356_61788490_1930998.png "微信图片_20190320095513.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101403_39c1c27c_1930998.jpeg "微信图片_20190320095513.jpg")

后台：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101441_da4ed3ff_1930998.png "数据1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101456_eb418bb2_1930998.png "微信截图_20190320095033.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0320/101511_771bfe94_1930998.png "微信截图_20190320094844.png")
